In near future this simple thing will be able to generate SVG's from ZPL2 format, which will 100% cover ZPL language possibilities and implementation.

For now... it does like 10% of that :)

If you need it/want to contribute, feel free to do that!
Any help is appreciated!

TODO:
- Implement whole barcodes generation.
- Move zpltosvg and rewrite it to use as bin script.
- Prepare setup.py, allow to use it as library.
- ???
- PROFIT
