import argparse
from zpltosvg.parsezpl import ZPLSVGGenerator

parser = argparse.ArgumentParser(description='Covert ZPL label to SVG')
parser.add_argument('zpl_file', help='ZPL input file')
parser.add_argument('svg_file', help='SVG output file')

args = parser.parse_args()

zpl_fh = open(args.zpl_file)
zpl_data = zpl_fh.read()
zpl_fh.close()

zplparser = ZPLSVGGenerator(zpl_data, args.svg_file)
zplparser.parse()
