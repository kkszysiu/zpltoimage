from PIL import Image, ImageDraw, ImageFont

import StringIO
import binascii

img_base = "P4\n%s %s\n%s"

class ImageHandler(object):
    @classmethod
    def from_ascii_hex(cls, bin_bytes, data, d=None):
        bin_data = binascii.unhexlify(data)

        if len(bin_data) != bin_bytes:
            raise Exception("length of binary data is not equal to bin_bytes * 2 for this graphics field type")

        #print 'd', d
        #print "len(bin_data)", len(bin_data)
        #print "bin_bytes", bin_bytes

        pw = d * 8
        ph = (len(bin_data) * 8)/pw
        #print pw, ph

        bitmap_data = img_base % (pw, ph, bin_data)

        img = Image.open(StringIO.StringIO(bitmap_data))
        return img



