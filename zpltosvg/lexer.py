from token import BaseBox, Token


KEYWORDS = {
    '^XA': 'C_XA',
    '^XZ': 'C_XZ',
    '^MC': 'C_MC',
    '^LH': 'C_LH',
    '^FO': 'C_FO',
    '^FT': 'C_FT',
    '^FS': 'C_FS',
    '^FW': 'C_FW',
    '^CF': 'C_CF',
    '^CI': 'C_CI',
    '^PR': 'C_PR',
    '^PO': 'C_PO',
    '^PM': 'C_PM',
    '^GB': 'C_GB',
    '^PQ': 'C_PQ',
    '^BC': 'C_BC',
    '^BY': 'C_BY',
    '^B3': 'C_B3',
    '^LL': 'C_LL',
    '^FR': 'C_FR',
    '^SN': 'C_SN',
    '^XG': 'C_XG',
    '^GS': 'C_GS',
    '^MT': 'C_MT',
    '^MM': 'C_MM',
    '^MN': 'C_MN',
    '^MD': 'C_MD',
    '^FH': 'C_FH',
    '^A': 'C_A',
    '^AO': 'C_AO',
    '^LT': 'C_LT',
    '^JM': 'C_JM',
    '^JU': 'C_JU',
    '^PW': 'C_PW',
    '^LS': 'C_LS',
    '^DFR': 'C_DFR',
    '^DFT': 'C_DFT',
    '^XFR': 'C_XFR',
    '^LRN': 'C_LRN',
    '^LR': 'C_LR',
}

RULES = [
    ("^XA", 'C_XA'),
    ("^XZ", 'C_XZ'),
    ('^GF', 'C_GF'),
    ("^MC", 'C_MC'),
    ("^LH", 'C_LH'),
    ("^FO", 'C_FO'),
    ("^FT", 'C_FT'),
    ("^FD", 'C_FD'),
    ("^FS", 'C_FS'),
    ("^FV", 'C_FV'),
    ("^FW", 'C_FW'),
    ("^CF", 'C_CF'),
    ("^CI", 'C_CI'),
    ("^PR", 'C_PR'),
    ("^PO", 'C_PO'),
    ("^PM", 'C_PM'),
    ("^GB", 'C_GB'),
    ("^PQ", 'C_PQ'),
    ("^BC", 'C_BC'),
    ("^BY", 'C_BY'),
    ("^B3", 'C_B3'),
    ("^BD", 'C_BD'),
    ("^LL", 'C_LL'),
    ("^FR", 'C_FR'),
    ("^FX", 'C_FX'),
    ("^FB", 'C_FB'),
    ("^FN", 'C_FN'),
    ("^SN", 'C_SN'),
    ("^XG", 'C_XG'),
    ("^XF", 'C_XF'),
    ("^GS", 'C_GS'),
    ("^MT", 'C_MT'),
    ("^MM", 'C_MM'),
    ("^MN", 'C_MN'),
    ("^MF", 'C_MF'),
    ("^MD", 'C_MD'),
    ("^FH", 'C_FH'),
    ("^A", 'C_A'),
    ("^AO", 'C_AO'),
    ("^LT", 'C_LT'),
    ("^JM", 'C_JM'),
    ("^JU", 'C_JU'),
    ("^PW", 'C_PW'),
    ("^LS", 'C_LS'),
    ("^DFR", 'C_DFR'),
    ("^DFT", 'C_DFT'),
    ("^XFR", 'C_XFR'),
    ("^LRN", 'C_LRN'),
    ("^LR", 'C_LR'),
    ("^DN", 'C_DN'),
    ("^CV", 'C_CV'),

    ("\\n", 'H_NEW_LINE'),
    ("\\t", 'H_TABULATURE'),
    (" ", 'H_WHITESPACE')]

#RULES = KEYWORDS.items() + RULES


class LexerError(Exception):
    """ Lexer error exception.

        pos:
            Position in the input line where the error occurred.
    """
    def __init__(self, pos):
        self.pos = pos


class Lexer(object):
    """ A simple regex-based lexer/tokenizer.

        See below for an example of usage.
    """
    def __init__(self, rules):
        """ Create a lexer.

            rules:
                A list of rules. Each rule is a `cmd, type`
                pair, where `cmd` is the regular sring used
                to recognize the command and `type` is the type
                of the token to return when it's recognized.
        """
        self.rules = []

        for cmd, type in rules:
            self.rules.append((cmd, type))

    def input(self, buf, pos, lineno):
        """ Initialize the lexer with a buffer as input.
        """
        buf = buf.replace("\n", "").replace("\r", "")
        self.buf = buf
        self.pos = pos
        self.lineno = lineno

    def token(self):
        """ Return the next token (a Token object) found in the
            input buffer. None is returned if the end of the
            buffer was reached.
            In case of a lexing error (the current chunk of the
            buffer matches no rule), a LexerError is raised with
            the position of the error.
        """
        if self.pos >= len(self.buf):
            return None
        else:
            while 1:
                start_point = self.buf.find('^', self.pos)
                if start_point != -1:
                    stop_point = self.buf.find('^', self.pos+1)
                    if stop_point == -1:
                        stop_point = len(self.buf)

                    cmd = self.buf[start_point:stop_point]
                    for token_cmd, token_type in self.rules:
                        if (cmd.startswith(token_cmd)):
                            argsstr = cmd[len(token_cmd):]
                            if token_type in ['C_FD', 'C_FV', 'C_FX']:
                                args = [argsstr]
                            else:
                                if argsstr:
                                    args = argsstr.split(",")
                                else:
                                    args = []

                            token = Token(token_type, token_cmd, arguments=args, source_pos=self.pos)
                            self.pos = stop_point

                            start_point = None
                            stop_point = None
                            return token

                    # if we're here, no rule matched
                    raise LexerError(self.pos)
                else:
                    break

    def tokens(self):
        """ Returns an iterator to the tokens found in the buffer.
        """
        while 1:
            tok = self.token()
            if tok is None:
                break
            yield tok


#class Lexer(object):
#    """ A simple regex-based lexer/tokenizer.
#
#        See below for an example of usage.
#    """
#    def __init__(self, rules, skip_whitespace=False):
#        """ Create a lexer.
#
#            rules:
#                A list of rules. Each rule is a `regex, type`
#                pair, where `regex` is the regular expression used
#                to recognize the token and `type` is the type
#                of the token to return when it's recognized.
#
#            skip_whitespace:
#                If True, whitespace (\s+) will be skipped and not
#                reported by the lexer. Otherwise, you have to
#                specify your rules for whitespace, or it will be
#                flagged as an error.
#        """
#        from re import compile, M, DOTALL
#        self.rules = []
#
#        for regex, type in rules:
#            self.rules.append((compile(regex, M | DOTALL), type))
#
#        self.skip_whitespace = skip_whitespace
#        self.re_ws_skip = compile('\S')
#
#    def input(self, buf, pos, lineno):
#        """ Initialize the lexer with a buffer as input.
#        """
#        self.buf = buf
#        self.pos = pos
#        self.lineno = lineno
#
#    def token(self):
#        """ Return the next token (a Token object) found in the
#            input buffer. None is returned if the end of the
#            buffer was reached.
#            In case of a lexing error (the current chunk of the
#            buffer matches no rule), a LexerError is raised with
#            the position of the error.
#        """
#        if self.pos >= len(self.buf):
#            return None
#        else:
#            if self.skip_whitespace:
#                m = self.re_ws_skip.search(self.buf[self.pos:])
#                if m:
#                    self.pos += m.start()
#                else:
#                    return None
#
#            for token_regex, token_type in self.rules:
#                m = token_regex.match(self.buf[self.pos:])
#                if m:
#                    print "m.start()", m.start()
#                    print "m.start()", m.start()
#                    value = self.buf[self.pos + m.start():self.pos + m.end()]
#                    if token_type == 'H_NEW_LINE':
#                        self.lineno += 1
#                    elif token_type == 'T_COMMENT':
#                        self.lineno += value.count('\n')
#                    elif token_type == 'T_STRING':
#                        keyword = KEYWORDS.get(value)
#                        if keyword is not None:
#                            token_type = keyword
#                    tok = Token(token_type, value, self.lineno)
#                    self.pos += m.end()
#                    return tok
#
#            # if we're here, no rule matched
#            raise LexerError(self.pos)
#
#    def tokens(self):
#        """ Returns an iterator to the tokens found in the buffer.
#        """
#        while 1:
#            tok = self.token()
#            if tok is None:
#                break
#            while tok.name in ('H_NEW_LINE', 'H_WHITESPACE',
#                               'T_COMMENT', 'H_TABULATURE'):
#                tok = self.token()
#                if tok is None:
#                    break
#            yield tok

#if __name__ == "__main__":
#    l = Lexer(RULES)
#    buf = """
#$a = <<<e333
#345345345
#ewre " sdfsd sdf" sd"'';;;""
#e333;
#var_dump($a);
#"""
#    l.input(buf, 0, 0)
#    for t in l.tokens():
#        print t
#        print t.gettokentype()
