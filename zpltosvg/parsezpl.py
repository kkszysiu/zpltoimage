from lexer import Lexer, RULES

import svgwrite

from imagehandler import ImageHandler

from elaphe.maxicode import MaxiCode
from elaphe.code128 import Code128

import base64
import logging
import collections

logging.basicConfig()
logger = logging.getLogger('zpltosvg')
logger.setLevel(logging.DEBUG)


class ParseZPL(object):
    def __init__(self, source):
        self.source = source
        self.lexer = Lexer(RULES)
        self.lexer.input(self.source, 0, 0)

    def parse(self):
        for token in self.lexer.tokens():
            handler_func = "handle%s" % token.name

            logger.debug("Calling %s" % handler_func)
            logger.debug("Params: %r" % token.getargs())
            if hasattr(self, handler_func):
                getattr(self, handler_func)(*token.getargs())
            else:
                logging.warning(str(token) + " handling is not implemented yet.")
                exit(1)


class ZPLSVGGenerator(ParseZPL):
    def __init__(self, source, save_to):
        super(ZPLSVGGenerator, self).__init__(source)
        self.save_to = save_to
        self.is_reverse_print = False
        self.label_home = [0, 0]
        self.field_origin = [0, 0, 0]
        self.field_typeset = [0, 0, 0]

        self.a = collections.namedtuple('ScalableBitmappedFont', 'fo f o h w')
        self.by = None

        self.bd_bar_code_parsing = False
        self.bd_bar_code = collections.namedtuple('UPSMaxiCodeBarCode', 'm n t')

        self.bc_bar_code_parsing = False
        self.bc_bar_code = None

        self.dwg = None
        self.dwg_bg = "white"

        self.field_hex_indicator = None

    def handleC_XA(self):
        # Start of label
        self.dwg = svgwrite.Drawing(self.save_to, (2048, 4096))

    def handleC_XZ(self):
        # Start of label
        self.dwg.save()

    def handleC_LR(self):
        # Label Reverse Print On
        self.is_reverse_print = True

    def handleC_LRN(self):
        # Label Reverse Print Off
        self.is_reverse_print = False

    def handleC_GB(self, w=1, h=1, t=1, c='B', r=0):
        """
            Graphic Box

            w = box width (in dots)
            h = box height (in dots)
            t = border thickness (in dots)
            c = line color
            r = degree of corner-rounding
        """
        logger.info("Drawing graphic box: w %s, h %s, t %s, c %s, r %s" % (w, h, t, c, r))

        t = int(t)
        # quite a hack
        # TODO: describe hack
        color = (255, 255, 255)
        stroke_color = (0, 0, 0)
        if c == "W":
            color = (0, 0, 0)
            stroke_color = (255, 255, 255)

        if r != 0:
            raise NotImplementedError("Rounding is not implemented yet")

        if int(w) == 0:
            w = 1
        elif int(h) == 0:
            h = 1

        logger.info(dict(
            insert=(self.field_origin[0], self.field_origin[1]),
            size=(int(w), int(h)),
            fill=svgwrite.utils.rgb(*color),
            stroke=svgwrite.utils.rgb(*stroke_color),
            stroke_width=t
        ))

        self.dwg.add(self.dwg.rect(
            insert=(self.field_origin[0], self.field_origin[1]-t),
            size=(int(w), int(h)),
            fill=svgwrite.utils.rgb(*color),
            stroke=svgwrite.utils.rgb(*stroke_color),
            stroke_width=t
        ))
        #dwg.add(dwg.g(id='shapes', fill='red'))


    def handleC_FS(self):
        """
            Field Separator
        """
        logger.info("Some Field Separator shit. Should we do nything here?")

    def handleC_A(self, fo=None, h=None, w=None):
        """
            Scalable/Bitmapped Font

            f = font name
            o = field orientation
            h = Character Height (in dots)
            w = width (in dots)
        """
        logger.info("Some Scalable/Bitmapped Font shit. I dont get what it does. Can we implement it later? fo: %r h: %r, w: %r" % (fo, h, w))
        self.a.fo = fo
        self.a.h = h
        self.a.w = w

    def handleC_FD(self, a=None):
        """
            Field Data

            a = data to be printed
        """
        def handle_field_hex_indicators(a):
            if self.field_hex_indicator:
                while a.find(self.field_hex_indicator) != -1:
                    hex_pos = a.find(self.field_hex_indicator)
                    hex_expr = a[hex_pos:hex_pos + 3]
                    str_val = a[hex_pos + 1:hex_pos + 3].decode("hex")
                    a = a.replace(hex_expr, str_val)
            return a

        a = handle_field_hex_indicators(a)

        if self.bd_bar_code_parsing:
            logger.info("Handle bd_bar_code_parsing someday")

            def replace_special_chars(lpm):
                lpm = lpm.replace('\x1d', '^029')
                lpm = lpm.replace('\x1e', '^030')
                lpm = lpm.replace('\x04', '^004')
                
                return lpm

            bc = MaxiCode()
            #img = bc.render(
                #'ABC123^029840^029001^0291Z00004951^029UPSN^02906X610'
                #'^029159^0291234567^0291/1^029^029Y^029634 ALPHA DR^029'
                #'PITTSBURGH^029PA^029^004',
                #options=dict(mode=3, margin=1, scale=10, parse=True)
            #)
            print "a", a
            print "a", repr(a)
            print "replace_special_chars a", repr(replace_special_chars(a))
            print "self.bd_bar_code.m", self.bd_bar_code.m
            hpm = ''
            lpm = a
            print 'self.bd_bar_code.m == int(2)', int(self.bd_bar_code.m) == int(2)
            if int(self.bd_bar_code.m) == 2:
                hpm = a[:15]
                lpm = a[15:]
            elif int(self.bd_bar_code.m) == 3:
                hpm = a[:12]
                lpm = a[12:]

            print "hpm", hpm
            print "lpm", lpm

            """
            img = bc.render(
                "001840152382802[)>^03001^029961Z00004951^029UPSN^029\x06X610^029159^0291234567^0291/1^029^029Y^029634 ALPHA DR^029PITTSBURGH^029PA^030^004",
                options=dict(mode=int(self.bd_bar_code.m)),
                parse=True
            )

            img.save('barcode.png', 'png')
            """

            self.bd_bar_code_parsing = False
        elif self.bc_bar_code_parsing:
            print 'bc_bar_code_parsing!!!'
            print self.bc_bar_code
            height = 0
            interpretation_line = False if self.bc_bar_code.f == 'N' else True
            interpretation_line_above = True if self.bc_bar_code.g == 'Y' else False
            ucc_check_digit = True if self.bc_bar_code.f == 'Y' else False

            if self.bc_bar_code.h:
                height = int(self.bc_bar_code.h)
            else:
                #  value set by ^BY
                raise NotImplementedError

            if self.bc_bar_code.o == 'N':  # normal orientation
                if self.bc_bar_code.m == 'A':  # automatic mode
                    bc = Code128()
                    print a
                    img = bc.render(
                        #'^104^102Count^0990123456789^101!',
                        a,
                        #options=dict(height=height),
                        parse=True
                    )
                    print height
                    img.save('barcode.png', 'png')
                    #print bc.render_ps_code('^104^102Count^0990123456789^101!')
                else:
                    raise NotImplementedError
            else:
                raise NotImplementedError
            self.bc_bar_code_parsing = False
            #exit(1)
        else:
            if not self.field_typeset[0] and not self.field_typeset[1]:
                x_pos = self.field_origin[0]
                y_pos = self.field_origin[1]
            else:
                x_pos = self.field_typeset[0]
                y_pos = self.field_typeset[1]
            pos = (x_pos, y_pos)

            if self.a.w:
                font_size = self.a.w
            else:
                font_size = 14

            paragraph = self.dwg.add(self.dwg.g(font_size=font_size))
            paragraph.add(self.dwg.text(a, pos))

            logger.info("Drawing text: %s on position: %r" % (a, pos))

    def handleC_MN(self, a=None, b=None):
        """
            Media Tracking

            N = continuous media
            Y = non-continuous media web sensing k, l
            W = non-continuous media web sensing k, l
            M = non-continuous media mark sensing
            A = auto-detects the type media during cofV = continuous media, variable length n
        """
        logger.info("Some media tracking shit. I dont get what it does. Can we implement it later? a: %r b: %r" % (a, b))
        #raise NotImplementedError("C_MN")

    def handleC_MF(self, p=None, h=None):
        # Media Feed
        logger.info("Some media feed shit. I dont get what it does. Can we implement it later? p: %r h: %r" % (p, h))
        #raise NotImplementedError("C_MF")

    def handleC_LH(self, x=None, y=None):
        """
            Label Home

            x = x-axis position (in dots)
            y = y-axis position (in dots)
        """
        self.label_home = [int(x), int(y)]
        logger.info("Setting label home: %r" % self.label_home)

    def handleC_MC(self, a="Y"):
        """
            Map Clear

            a = map clear
        """
        logger.info("Some map get shit. I dont get what it does. Can we implement it later? a: %r" % (a))
        #raise NotImplementedError("C_MC")

    def handleC_PO(self, a="N"):
        """
            Print Orientation

            a = invert label 180 degrees
        """
        if a == "I":
            # Couldnt we just invert it after we draw everythig?
            logger.error("Print Orientation a: %r" % (a))

            #raise NotImplementedError("C_PO I is not implemented yet")

    def handleC_PW(self, a=None):
        """
            Print Width

            a - label width
        """
        width = int(a)
        #self.im = Image.new(self.im_type, (width, 1024), self.im_bg)
        self.dwg = svgwrite.Drawing(self.save_to, (width, 4096))
        logger.info("Setting printing width: %i" % width)

    def handleC_CI(self, a=None, *args, **kwargs):
        """
            Change International Font/Encoding
            a -  desired character set

            0 = Single Byte Encoding - U.S.A. 1 Character Set
            1 = Single Byte Encoding - U.S.A. 2 Character Set
            2 = Single Byte Encoding - U.K. Character Set
            3 = Single Byte Encoding - Holland Character Set
            4 = Single Byte Encoding - Denmark/Norway Character Set
            5 = Single Byte Encoding - Sweden/Finland Character Set
            6 = Single Byte Encoding - Germany Character Set
            7 = Single Byte Encoding - France 1 Character Set
            8 = Single Byte Encoding - France 2 Character Set
            9 = Single Byte Encoding - Italy Character Set
            10 = Single Byte Encoding - Spain Character Set
            11 = Single Byte Encoding - Miscellaneous Character Set
            12 = Single Byte Encoding - Japan (ASCII with Yen symbol) Character Set
            13 = Zebra Code Page 850
            14 = Double Byte Asian Encodings a
            15 = Shift-JIS b
            16 = EUC-JP and EUC-CN a
            17 = Deprecated - UCS-2 Big Endian d
            18 to 23 = Reserved
            24 = Single Byte Asian Encodings a
            25 = Reserved
            26 = Multibyte Asian Encodings with ASCII Transparency a c
            27 = Zebra Code Page 1252
            28 = Unicode (UTF-8 encoding) - Unicode Character Set
            29 = Unicode (UTF-16 Big-Endian encoding) - Unicode Character Set
            30 = Unicode (UTF-16 Little-Endian encoding) - Unicode Character Set
            31 = Zebra Code Page 1250 (see page 1078) is supported for scalable
            fonts, such as Font 0, or a downloaded TrueType font. Bitmapped
            fonts (including fonts A-H) do not fully support Zebra Code Page
            1250. This value is supported only on Zebra G-SeriesTM printers.
            33 = Code Page 1251
            34 = Code page 1253
            35 = Code Page 1254
            36 = Code Page 1255

        """
        logger.error("Change International Font/Encoding is not implemented at all yet...")
        #raise NotImplementedError("C_CI")

    def handleC_FO(self, x=None, y=None, z=None):
        """
            Field Origin
            x - x-axis location (in dots)
            y - y-axis location (in dots)
            z - justification
        """
        logger.info("Setting field origin: x %s, y %s, z %s" % (x, y, z))
        self.field_origin = [self.label_home[0] + int(x), self.label_home[1] + int(y), z]

    def handleC_GF(self, a="A", b=None, c=None, d=None, data=None):
        """
            Graphic Field

            a - compression type
            b - binary byte count
            c - graphic field count
            d - bytes per row
            data - data
        """
        if not all([b, c, d]):
            return False

        # could be ascii_hex, bin or compressed_bin
        logger.info("Graphic Field a: %r, b: %r, c: %r, d: %r, data: %r" % (a, b, c, d, data))

        b = int(b)
        c = int(c)
        d = int(d)

        img_data = None

        if a == 'A':
            if b != c:  # this should be equal for this type
                exception_msg = 'b value should be equal to' \
                                'c for this graphics field type'
                raise Exception(exception_msg)

            img_data = ImageHandler.from_ascii_hex(b, data, d=d)

        elif a == 'B':
            msg = 'C_GF binary data handling is not implemented yet'
            raise NotImplementedError(msg)
        elif a == 'C':
            msg = 'C_GF compressed binary data handling is not implemented yet'
            raise NotImplementedError(msg)

        pos = (self.field_origin[0], self.field_origin[1])

        import StringIO

        #img_data.save("test.png", format='PNG')
        output = StringIO.StringIO()
        img_data.save(output, format='PNG')
        contents = output.getvalue()
        contents = "data:image/png;base64,%s" % base64.b64encode(contents)
        output.close()

        #self.dwg.add(self.dwg.image(img_fn, insert=(-0.5,-0.5), size=(sx, sy))
        logging.info("img_data.size")
        logging.info(img_data.size)
        self.dwg.add(self.dwg.image(contents, insert=pos, size=img_data.size))

    def handleC_FH(self, a="_"):
        """
            Field Hexadecimal Indicator

            a = hexadecimal indicator

        """
        #raise NotImplementedError("C_FH")
        self.field_hex_indicator = a

    def handleC_FV(self, a=None):
        """
            Field Variable

            a = variable field data to be printed
        """
        logger.info("Some primitive Field Variable shit. I dont get what it does. Can we implement it later? a: %r" % a)
        self.handleC_FD(a)

    def handleC_DN(self):
        """
            UNKNOWN
        """
        logger.info("Some DN shit. I dont get what it does. It's not in DOCS too! Can we implement it later?")

    def handleC_FT(self, x=0, y=0, z=0):
        """
            Field Typeset

            x = x-axis location (in dots)
            y = y-axis location (in dots)
            z = justification
        """
        logger.info("Some Field Typeset shit. I dont get what it does. Can we implement it later? x: %r, y: %r, z: %r" % (z, y, z))
        self.field_typeset = [int(x), int(y), int(z)]

    def handleC_CV(self, a="N"):
        """
            Code Validation

            a = code validation
        """
        logger.info("Some Code Validation shit. I dont get what it does. Can we implement it later? a: %r" % a)

    def handleC_CF(self, f=None, h=None, w=None, _=None):
        """
            Change Alphanumeric Default Font

            f = specified default font
            h = individual character height (in dots)
            w = individual character width (in dots)
        """
        # There is lexer error...
        logger.info("Some Change Alphanumeric Default Font shit. I dont get what it does. Can we implement it later? f: %r, h: %r, w: %r" % (f, h, w))

    def handleC_MD(self, c=None):
        """
            Media Darkness

            c = non printing comment
        """
        logger.info("Media Darkness: %s" % c)

    def handleC_PR(self, c=None):
        """
            Print Rate

            c = non printing comment
        """
        logger.info("Print Rate: %s" % c)


    def handleC_FX(self, c=None):
        """
            Comment

            c = non printing comment
        """
        logger.info("Comment: %s" % c)

    def handleC_FN(self, no=0, a=None):
        """
            Field Number

            # = number to be assigned to the field
            "a" = optional parameter
        """
        raise NotImplementedError("C_FN")

    def handleC_FB(self, a=0, b=1, c=0, d='L', e=0):
        """
            Field Block

            a = width of text block line (in dots)
            b = maximum number of lines in text block
            c = add or delete space between lines (in dots)
            d = text justification
            e = hanging indent (in dots) of the second and remaining lines
        """
        raise NotImplementedError("C_FB")

    def handleC_XF(self, d=None, o=None, x=None):
        """
            Recall Format

            d = source device of stored image
            o = name of stored image
            x = extension l
        """
        raise NotImplementedError("C_XF")

    """ BARCODE RELATED STUFF """

    def handleC_BY(self, w=None, r=None, h=None):
        """
            Bar Code Field Default

            w = module width (in dots)
            r = wide bar to narrow bar width ratio
            h = bar code height (in dots)
        """
        by_tuple = collections.namedtuple('BarCodeFieldDefault', 'w r h')
        self.by = by_tuple(w, r, h)

    def handleC_BC(self, o=None, h=None, f=None, g=None, e=None, m=None):
        """
            Code 128 Bar Code (Subsets A, B, and C)

            o = orientation
            h = bar code height (in dots)
            f = print interpretation line
            g = print interpretation line above code
            e = UCC check digit
            m = mode
        """
        logging.info('Code 128 Bar Code')
        self.bc_bar_code_parsing = True
        bc_tuple = collections.namedtuple('Code128BarCode', 'o h f g e m')
        self.bc_bar_code = bc_tuple(o, h, f, g, e, m)

    def handleC_B3(self, o=None, e=None, h=None,f=None, g=None):
        """
            Code 39 Bar Code

            o = orientation
            e = Mod-43 check digit
            h = bar code height (in dots)
            f = print interpretation line
            g = print interpretation line above code

        """
        pass

    def handleC_BD(self, m=None, n=None, t=None):
        """
            UPS MaxiCode Bar Code

            m = mode
            n = symbol number
            t = total number of symbols
        """
        logger.info("UPS MaxiCode Bar Code")
        self.bd_bar_code_parsing = True
        print m, n, t
        self.bd_bar_code.m = m or 2
        self.bd_bar_code.n = n
        self.bd_bar_code.t = t

    def handleC_DFT(self):
        pass
