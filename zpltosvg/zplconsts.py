

class ZPLConstants(object):
    EOF = 0
    C_XA = 5
    C_XZ = 6
    C_MC = 7
    C_LH = 8
    C_FO = 9
    C_FT = 10
    C_FS = 11
    C_LR = 12
    C_FW = 13
    C_CF = 14
    C_CI = 15
    C_PR = 16
    C_PO = 17
    C_PM = 18
    C_GB = 19
    C_PQ = 20
    C_BC = 21
    C_BY = 22
    C_B3 = 23
    C_LL = 24
    C_FR = 25
    C_SN = 26
    C_XG = 27
    C_GS = 28
    C_MT = 29
    C_MM = 30
    C_MN = 31
    C_MD = 32
    C_FH = 33
    C_A = 34
    C_AO = 35
    C_LT = 36
    C_JM = 37
    C_JU = 38
    C_PW = 39
    C_LS = 40
    C_DFR = 41
    C_XFR = 42
    C_XF = 43
    C_EG = 44
    P_EG = 45
    P_TA = 46
    P_JS = 47
    P_SD = 48
    P_DG = 49
    C_FD = 50
    C_ID = 51
    COLON = 52
    GRF = 53
    CHAR = 54
    small_char = 55
    DIGIT = 56
    NUMBER = 57
    FLOAT = 58
    STRING = 59

    DEFAULT = 0
    VALUE = 1

    tokenImage = [
        "<EOF>",
        "\" \"",
        "\"\\n\"",
        "\"\\r\"",
        "\"\\r\\n\"",
        "\"^XA\"",
        "\"^XZ\"",
        "<C_MC>",
        "\"^LH\"",
        "\"^FO\"",
        "\"^FT\"",
        "\"^FS\"",
        "\"^LR\"",
        "\"^FW\"",
        "\"^CF\"",
        "\"^CI\"",
        "\"^PR\"",
        "\"^PO\"",
        "\"^PM\"",
        "\"^GB\"",
        "\"^PQ\"",
        "\"^BC\"",
        "\"^BY\"",
        "\"^B3\"",
        "\"^LL\"",
        "\"^FR\"",
        "\"^SN\"",
        "\"^XG\"",
        "\"^GS\"",
        "\"^MT\"",
        "\"^MM\"",
        "\"^MN\"",
        "\"^MD\"",
        "\"^FH\"",
        "\"^A\"",
        "<C_AO>",
        "\"^LT\"",
        "\"^JM\"",
        "\"^JU\"",
        "\"^PW\"",
        "\"^LS\"",
        "\"^DFR:\"",
        "\"^XFR:\"",
        "\"^XF\"",
        "\"^EG\"",
        "\"~EG\"",
        "\"~TA\"",
        "\"~JS\"",
        "\"~SD\"",
        "\"~DG\"",
        "\"^FD\"",
        "\"^ID\"",
        "\":\"",
        "\".GRF\"",
        "<CHAR>",
        "<small_char>",
        "<DIGIT>",
        "<NUMBER>",
        "<FLOAT>",
        "<STRING>",
        "\",\"",
    ]
